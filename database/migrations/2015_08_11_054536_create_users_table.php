<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('re_users', function (Blueprint $table) {
            $table->increments('id'); // user_id
            $table->string('email', 64)->unique(); // email = username
            $table->string('password', 64); // encrypted with bcrypt
            $table->integer('email_authenticated')->default(0); // changes to 1 when user has authenticated their email
            $table->rememberToken(); // used for password resets
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('re-users');
    }
}
